FROM python

WORKDIR /home/root

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

WORKDIR /home/root/Messenger

CMD sleep 10 \
  && python manage.py makemigrations \
  && python manage.py migrate \
  && daphne -b 0.0.0.0 -p 8000 Messenger.asgi:application

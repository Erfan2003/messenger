from channels.db import database_sync_to_async
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from rest_framework.authtoken.models import Token


@database_sync_to_async
def obtain_user(token_key):
    try:
        token = Token.objects.get(key=token_key)
        user = token.user
    except Token.DoesNotExist:
        user = AnonymousUser()
    return user


class TokenAuthMiddleware:
    """
    Custom middleware for token authentication with channels.
    """

    def __init__(self, inner):
        self.inner = inner

    def __call__(self, scope):
        close_old_connections()
        headers = dict(scope["headers"])
        if b"Authorization" in headers:
            token_key = headers[b"Authorization"].decode()
            user = obtain_user(token_key)
        else:
            user = AnonymousUser()
        return self.inner(dict(scope, user=user))

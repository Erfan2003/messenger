from channels.routing import ProtocolTypeRouter, URLRouter

from Auth import TokenAuthMiddleware
from core.routing import websocket_url_patterns

application = ProtocolTypeRouter(
    {"websocket": TokenAuthMiddleware(URLRouter(websocket_url_patterns))}
)

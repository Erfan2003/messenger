from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(User, models.CASCADE)
    name = models.CharField(max_length=220)
    photo = models.ImageField(upload_to="uploads/profile", blank=True)
    is_online = models.BooleanField(default=False)

    def change_is_online(self, value: bool):
        self.is_online = value
        self.save()

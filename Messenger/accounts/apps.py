from django.apps import AppConfig
from django.db.models.signals import post_save


class AccountsConfig(AppConfig):
    name = "accounts"

    def ready(self):
        from . import signals
        from django.contrib.auth.models import User

        post_save.connect(signals.create_user_profile, sender=User)
        post_save.connect(signals.create_token, sender=User)

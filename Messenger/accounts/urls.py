from django.urls import include, path, re_path
from rest_auth.views import PasswordResetConfirmView

urlpatterns = [
    path("", include("rest_auth.urls")),
    path("registration", include("rest_auth.registration.urls")),
    re_path(
        r"^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{"
        r"1,20})/$",
        PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
]

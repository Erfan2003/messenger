from rest_framework.authtoken.models import Token

from .models import Profile


def create_user_profile(instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance, name=instance.username)


def create_token(instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)

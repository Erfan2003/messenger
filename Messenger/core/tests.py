import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, force_authenticate

from Messenger.Messenger.routing import application

from .models import Chat, Message
from .views import ChatListView, ChatMessagesListView


@database_sync_to_async
def obtain_auth_token(username):
    user, _ = User.objects.get_or_create(username=username, password="Test")
    auth_token = Token.objects.get(user=user).key
    return auth_token


@database_sync_to_async
def get_user_is_online(username="Test"):
    user = User.objects.get(username=username)
    return user.profile.is_online


@database_sync_to_async
def create_chat():
    chat, _ = Chat.objects.get_or_create(title="Test", type="P")
    return chat.id


@database_sync_to_async
def get_chat_members_count():
    chat = Chat.objects.get()
    return chat.members.count()


async def get_authenticated_communicator(app, path, username="Test"):
    auth_token = await obtain_auth_token(username)
    headers = {b"Authorization": f"{auth_token}".encode()}
    communicator = WebsocketCommunicator(app, path, headers=headers)
    return communicator


@pytest.mark.django_db
@pytest.mark.asyncio
class TestCoreConsumer:
    async def test_close_on_anonymous_user(self):
        communicator = WebsocketCommunicator(application, "websocket")
        connected, _ = await communicator.connect()
        assert not connected
        await communicator.disconnect()

    async def test_connect_on_non_anonymous_user(self):
        communicator = await get_authenticated_communicator(application, "websocket")
        connected, _ = await communicator.connect()
        assert connected
        await communicator.disconnect()

    async def test_is_online(self):
        communicator = await get_authenticated_communicator(application, "websocket")
        await communicator.connect()
        is_online = await get_user_is_online()
        assert is_online
        await communicator.disconnect()
        is_online = await get_user_is_online()
        assert not is_online

    async def test_join_and_leave(self):
        communicator = await get_authenticated_communicator(application, "websocket")
        second_communicator = await get_authenticated_communicator(
            application, "websocket", username="Test2"
        )
        chat_id = await create_chat()
        await communicator.connect()
        await second_communicator.connect()
        await communicator.send_json_to({"command": "join", "chat_id": chat_id})
        msg = await communicator.receive_json_from()
        assert msg["sender"] == "Test"
        chat_members_count = await get_chat_members_count()
        assert chat_members_count == 1
        await second_communicator.send_json_to({"command": "join", "chat_id": chat_id})
        await second_communicator.receive_json_from()
        msg = await communicator.receive_json_from()
        assert msg["sender"] == "Test2"
        chat_members_count = await get_chat_members_count()
        assert chat_members_count == 2
        await communicator.send_json_to({"command": "leave", "chat_id": chat_id})
        msg = await second_communicator.receive_json_from()
        assert msg["sender"] == "Test"
        assert await get_chat_members_count() == 1
        await second_communicator.send_json_to({"command": "leave", "chat_id": chat_id})
        await second_communicator.receive_nothing()
        assert await get_chat_members_count() == 0
        await communicator.disconnect()
        await second_communicator.disconnect()

    async def test_send_message(self):
        communicator = await get_authenticated_communicator(application, "websocket")
        second_communicator = await get_authenticated_communicator(
            application, "websocket", username="Test2"
        )
        chat_id = await create_chat()
        await communicator.connect()
        await second_communicator.connect()
        await communicator.send_json_to({"command": "join", "chat_id": chat_id})
        await communicator.receive_json_from()
        await second_communicator.send_json_to({"command": "join", "chat_id": chat_id})
        await communicator.receive_json_from()
        await second_communicator.receive_json_from()
        await communicator.send_json_to(
            {"command": "send", "chat_id": chat_id, "text": "Hello"}
        )
        msg = await communicator.receive_json_from()
        assert msg["text"] == "Hello"
        msg = await second_communicator.receive_json_from()
        assert msg["text"] == "Hello"
        await communicator.disconnect()
        await second_communicator.disconnect()


@pytest.mark.django_db
class TestViews:
    def test_chat_list_view(self):
        Chat.objects.all().delete()
        User.objects.all().delete()
        Message.objects.all().delete()
        chat = Chat.objects.create(title="Test")
        chat_2 = Chat.objects.create(title="Test2")
        user = User.objects.create(username="Test", password="Test")
        chat.members.add(user)
        chat_2.members.add(user)
        Message.objects.create(from_user=user, to_chat=chat, text="Test")
        Message.objects.create(from_user=user, to_chat=chat_2, text="Test3")
        Message.objects.create(from_user=user, to_chat=chat, text="Test2")
        Message.objects.create(from_user=user, to_chat=chat_2, text="Test4")
        factory = APIRequestFactory()
        request = factory.get("chats/")
        force_authenticate(request, user=user)
        response = ChatListView.as_view()(request=request)
        assert len(response.data) == 2
        assert response.data[0]["last_message"]["text"] == "Test4"
        assert response.data[1]["last_message"]["text"] == "Test2"
        assert response.data[0]["title"] == "Test2"
        assert response.data[1]["title"] == "Test"

    def test_chat_messages_list_view(self):
        user = User.objects.create(username="Test", password="Test")
        chat = Chat.objects.create(title="Test")
        chat.members.add(user)
        Message.objects.create(from_user=user, to_chat=chat, text="Test")
        Message.objects.create(from_user=user, to_chat=chat, text="Test2")
        factory = APIRequestFactory()
        request = factory.get("messages/1")
        force_authenticate(request, user=user)
        response = ChatMessagesListView.as_view()(request=request, chat_id=1)
        assert len(response.data) == 2
        assert response.data[0]["text"] == "Test"
        assert response.data[1]["text"] == "Test2"

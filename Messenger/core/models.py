from django.contrib.auth.models import User
from django.db import models


class Chat(models.Model):
    CHAT_TYPES = [
        ("P", "Private"),
        ("G", "Group"),
    ]
    members = models.ManyToManyField(User)
    title = models.CharField(max_length=70, blank=True)
    type = models.CharField(max_length=1, choices=CHAT_TYPES)

    def get_last_message(self):
        return self.messages.order_by('date').last()


class Message(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE)
    to_chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name="messages")
    text = models.CharField(max_length=2048)
    date = models.DateTimeField(auto_now=True)

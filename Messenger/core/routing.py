from django.urls import path

from .consumers import CoreConsumer

websocket_url_patterns = [
    path("websocket", CoreConsumer),
]

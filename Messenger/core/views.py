from rest_framework.generics import ListAPIView

from .models import Message
from .serializers import ChatSerializer, MessageSerializer


class ChatListView(ListAPIView):
    serializer_class = ChatSerializer

    def get_queryset(self):
        return self.request.user.chat_set.all()


class ChatMessagesListView(ListAPIView):
    serializer_class = MessageSerializer

    def get_queryset(self):
        return Message.objects.filter(to_chat__id=self.kwargs["chat_id"])

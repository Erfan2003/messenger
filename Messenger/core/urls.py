from django.urls import path

from .views import ChatListView, ChatMessagesListView

urlpatterns = [
    path("chats/", ChatListView.as_view()),
    path("messages/<int:chat_id>/", ChatMessagesListView.as_view()),
]

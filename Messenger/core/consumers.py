from types import CoroutineType

from channels.db import database_sync_to_async
from channels.exceptions import StopConsumer
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .models import Chat, Message


class CoreConsumer(AsyncJsonWebsocketConsumer):
    """
    Core consumer to handle most of the events.
    In near future it may break into multiple consumers for now we devise this strategy.
    """

    async def connect(self):
        """
        Called to handle initial handshake.
        :return: None
        """
        self.user = self.scope["user"]
        if isinstance(self.user, CoroutineType):
            self.user = await self.user
        if self.user.is_anonymous:
            await self.close()
            raise StopConsumer
        else:
            await self.accept()
        await self.change_is_user_online(self.user, True)
        user_chats = await self.get_user_chat_set(self.user)
        for chat in user_chats:
            await self.channel_layer.group_add(str(chat.id), self.channel_name)

    async def disconnect(self, code):
        """
        Called when for any reason websocket is disconnected.
        :param code: code value sent with message. (We don't need)
        :return: None
        """
        await self.change_is_user_online(self.user, False)
        user_chats = await self.get_user_chat_set(self.user)
        for chat in user_chats:
            await self.channel_layer.group_discard(str(chat.id), self.channel_name)
        raise StopConsumer

    async def receive_json(self, content, **kwargs):
        """
        Called to handle user json-encoded text.
        Good to note that channels decode this for us.
        :param content: json-decoded dict
        :param kwargs: Additional args
        :return: None
        """
        if not (message_type := content.get("command", None)):
            await self.send_json({"Error": "Message type is not declared."})
        if message_type == "send":
            await self.channel_layer.group_send(
                str(content["chat_id"]),
                {
                    "type": "chat.send",
                    "sender": f"{self.user.username}",
                    "chat": content["chat_id"],
                    "text": content["text"],
                },
            )
            await self.create_new_message(content["chat_id"], content["text"])
        elif message_type == "join":
            await self.channel_layer.group_add(
                str(content["chat_id"]), self.channel_name
            )
            await self.channel_layer.group_send(
                str(content["chat_id"]),
                {
                    "type": "chat.join",
                    "msg_action": "join",
                    "sender": self.user.username,
                    "chat": content["chat_id"],
                },
            )
            await self.update_group_members(content["chat_id"], True)
        elif message_type == "leave":
            await self.channel_layer.group_discard(
                str(content["chat_id"]), self.channel_name
            )
            await self.channel_layer.group_send(
                str(content["chat_id"]),
                {
                    "type": "chat.leave",
                    "msg_action": "leave",
                    "sender": self.user.username",
                    "chat": content["chat_id"],
                },
            )
            await self.update_group_members(content["chat_id"], False)
        elif message_type == "is_typing":
            await self.channel_layer.group_send(
                str(content["chat_id"]),
                {
                    "type": "is_typing",
                    "sender": self.user.username",
                    "chat": content["chat_id"],
                },
            )

    async def chat_join(self, event):
        """
        Called to broadcast the join event on every member of group.
        :param event:  A dictionary.
        :return: None
        """
        await self.send_json(event)

    async def chat_leave(self, event):
        """
        Called to broadcast the leave event on every member of group.
        :param event: A dictionary.
        :return: None
        """
        await self.send_json(event)

    async def chat_send(self, event):
        """
        Called to broadcast the message sent by one user to other members.
        :param event: A dictionary.
        :return: None
        """
        await self.send_json(event)

    @database_sync_to_async
    def create_new_message(self, chat_id, text):
        """
        Called when incoming type is send.
        :param chat_id: The chat id user want to send message to.
        :param text: The text of message.
        :return: None
        """
        Message.objects.create(
            from_user=self.user, to_chat=Chat.objects.get(id=chat_id), text=text
        )

    @database_sync_to_async
    def update_group_members(self, chat_id, is_join):
        """
        Called when incoming type is leave/join.
        :param chat_id: The chat id user joined or left.
        :param is_join: When user joined it is True else False.
        :return: None
        """
        if is_join:
            Chat.objects.get(id=chat_id).members.add(self.user)
        else:
            Chat.objects.get(id=chat_id).members.remove(self.user)

    @database_sync_to_async
    def change_is_user_online(self, user, value):
        user.profile.change_is_online(value)

    @database_sync_to_async
    def get_user_chat_set(self, user):
        return list(user.chat_set.all())  # Forcing the fetch in the sync.

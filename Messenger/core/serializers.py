from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Chat, Message


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username"]


class MessageSerializer(serializers.ModelSerializer):
    from_user = UserSerializer()

    class Meta:
        model = Message
        exclude = ["to_chat"]


class ChatSerializer(serializers.ModelSerializer):
    last_message = MessageSerializer(source="get_last_message")

    class Meta:
        model = Chat
        exclude = ["members"]
